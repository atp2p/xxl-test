package com.xxl.api.controller;


import com.xxl.api.service.IXxlApiDataTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 测试类
 * index controller
 * @author qj
 */
@Controller
public class IndexController {


	@Autowired
	private  IXxlApiDataTypeService xxlApiDataTypeService;

	/**
	 * 示例生成api文档方法
	 *
	 * @param productId 年龄
	 * @param guo       姓氏
	 * @return 商品库存DTO
	 */

	/**
	 * 示例加密方法
	 *
	 * @param desStr 原文
	 * @return 加密字符串
	 */
	@GetMapping("/encrypt/{desStr}")
	public ResponseEntity<String> encrypt(@PathVariable("desStr") String desStr ) {
		if(desStr==null) desStr="123";
		desStr=xxlApiDataTypeService.testEncrypt(desStr);

		return ResponseEntity.ok(desStr);
	}

	/**
	 * 示例 help方法
	 *
	 * @return
	 */
	@RequestMapping("/help")
	public String help() {
		return "help";
	}
	
}
