package com.xxl.api.service.impl;


import com.xxl.api.service.IXxlApiDataTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import com.bxlt.util.Encrpt;
/**
 * @author xuxueli
 */
@Service
public class XxlApiDataTypeServiceImpl implements IXxlApiDataTypeService {
	private static Logger logger = LoggerFactory.getLogger(XxlApiDataTypeServiceImpl.class);


	@Override
	public String testEncrypt(String entStr){
		Encrpt encrpt=new Encrpt();
		String enStr=encrpt.encrypt(entStr);
//		String  desStr=encrpt.decrypt(enStr);
		return enStr;
	}


}
