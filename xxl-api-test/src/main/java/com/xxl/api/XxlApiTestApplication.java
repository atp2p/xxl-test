package com.xxl.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author qj 2018-10-25
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.xxl.api"})
public class XxlApiTestApplication {

	public static void main(String[] args) {
        SpringApplication.run(XxlApiTestApplication.class, args);
	}

}