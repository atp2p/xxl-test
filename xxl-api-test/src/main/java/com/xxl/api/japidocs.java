package com.xxl.api;

import io.github.yedaxia.apidocs.DocsConfig;
import io.github.yedaxia.apidocs.Docs;
import io.github.yedaxia.apidocs.plugin.markdown.MarkdownDocPlugin;

public class japidocs {

    public   static void main(String[] args){
        DocsConfig config = new DocsConfig();
        config.setProjectPath("E:\\tbwkspace\\xxl-test\\xxl-api-test"); // 项目根目录
        config.setProjectName("xxl-api"); // 项目名称
        config.setApiVersion("V1.0");       // 声明该API的版本
        config.setDocsPath("E:\\tbwkspace\\xxl-test\\docs"); // 生成API 文档所在目录
        config.setAutoGenerate(Boolean.TRUE);  // 配置自动生成
        config.addPlugin(new MarkdownDocPlugin());
        Docs.buildHtmlDocs(config); // 执行生成文档
    }
}
